
# First rule of Dotfiles Club is to know which identity you are today
[ -f $HOME/.env ] && source $HOME/.env
[ -z "$EMAIL" ] && {
	echo "Please set EMAIL in ~/.env"
	[ -f $HOME/.env ] || {
		echo "Copy ~/.env-template to ~/.env and set email in that file"
	}
}

function command_exists {
	# Command should both exist and be executable
	[ -x "$(command -v $1)" ]
}

export GPG_TTY=$(tty)

mkdir -p $HOME/.completions
export FPATH=$HOME/.completions:$FPATH

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# In Iterm I use the built-in Status Bar
# In other programs I fallback to Avit
if [ "iTerm.app" != "$TERM_PROGRAM" ]; then
	#ZSH_THEME="avit"
fi

#ZSH_DISABLE_COMPFIX=true

# Auto correction is almost as annoying as T9
DISABLE_CORRECTION="true"

# Dots for the laugh
COMPLETION_WAITING_DOTS="true"

# Speed up Git status in repo
DISABLE_UNTRACKED_FILES_DIRTY="true"

# Please don't do that
setopt no_share_history

case `uname` in
  Darwin)
    alias flushdns='sudo dscacheutil -flushcache;sudo killall -HUP mDNSResponder;say cache flushed'
  ;;
  Linux)
	  alias a='sudo apt'
  ;;
esac

# Some magic stuff
alias -s txt=vim
alias -s java=vim
alias -s html=vim
alias -s css=vim
alias -s js=vim
alias -s go="go run"
alias luke="git push --force"
alias cdr='cd $(git rev-parse --show-toplevel)'
alias upgrade_gopls='go install golang.org/x/tools/gopls@latest'
alias upgrade_nix='sudo -i nix-channel --update nixpkgs && nix-channel --update darwin && darwin-rebuild switch'
alias gc_nix='nix-store --gc && nix-collect-garbage -d'
alias update_main='git fetch origin main:main'
alias rebase_updated_main='update_main && git rebase main && echo "🤘" || echo "😩"'
alias update_develop='git fetch origin develop:develop'
alias rebase_updated_develop='update_develop && git rebase develop && echo "🤘" || echo "😩"'
alias :q='exit'
alias d='cd ~/Developer'
alias rf='cd ~/Developer/rf/'
alias sf='cd ~/Developer/sf/'
alias gotmp='go build -o /tmp/gotemp-binary'

if command_exists gh; then
	[ -f ~/.completions/_gh ] || { gh completion -s zsh > ~/.completions/_gh }
	alias ghis="gh issue status"
	alias ghil="gh issue list"
	alias ghiv="gh issue view"
fi

#command -v ccat > /dev/null 2>&1 && alias cat=ccat

alias dgit='git --git-dir=$HOME/Developer/dotfiles/ --work-tree=$HOME'
alias dtig="GIT_DIR=$HOME/Developer/dotfiles/ GIT_WORK_TREE=$HOME tig"

blame() {
	tig blame -C "$*"
}

get_script_dir () {
     SOURCE="${BASH_SOURCE[0]}"
     # While $SOURCE is a symlink, resolve it
     while [ -h "$SOURCE" ]; do
          DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
          SOURCE="$( readlink "$SOURCE" )"
          # If $SOURCE was a relative symlink (so no "/" as prefix, need to resolve it relative to the symlink base directory
          [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
     done
     DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
     echo "$DIR"
}

plugins=(git)
plugins=(git)
plugins+=(docker)
plugins+=(docker-compose)
plugins+=(sudo)
plugins+=(tig)
# k is for kubectl # plugins+=(k)

source $ZSH/oh-my-zsh.sh

autoload -U compinit
compinit -i

export VISUAL="vim"

export GREP_COLORS='mt=33'

# Some programs expect this variable to be set even though dot is in the path
if [ -f /opt/local/bin/dot ]; then
	export GRAPHVIZ_DOT=/opt/local/bin/dot
else
    export GRAPHVIZ_DOT=$(which dot)
fi

# Go
export GIT_GET_PATH=$HOME/go/src

# Add my own stuff directories to the path
export PATH=$HOME/bin:$HOME/.local/bin::$HOME/go/bin:$PATH

export MANPATH=$HOME/.local/share/man:
export LANG=da_DK.UTF-8

# if [ -f /usr/libexec/java_home ]; then
# 	export JAVA_HOME=$(/usr/libexec/java_home > /dev/null 2>&1)
# fi

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
[ -f  /opt/local/share/fzf/shell/key-bindings.zsh ] && source /opt/local/share/fzf/shell/key-bindings.zsh

if [ -n "${commands[fzf-share]}" ]; then
  source "$(fzf-share)/key-bindings.zsh"
  source "$(fzf-share)/completion.zsh"
fi


if command_exists powerprofilesctl; then
	alias power=powerprofilesctl
fi

[ -f ~/.extra ] && source ~/.extra

test -f "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

if command_exists kubectl; then
	source <(kubectl completion zsh)
	alias k=kubectl
	alias K=kubecolor
	compdef kubecolor=kubectl
fi

if [ -d ~/.fly ]; then
	export FLYCTL_INSTALL="$HOME/.fly"
	export PATH="$PATH:$FLYCTL_INSTALL/bin"]
fi

if command_exists flyctl; then
	source <(flyctl completion zsh)
	#export PATH=$PATH:$HOME/.fly/bin
fi

# https://unix.stackexchange.com/a/10065
# if stdout is a terminal
if test -t 1; then
    # see if it supports colors
    ncolors=$(tput colors)
    if test -n "$ncolors" && test $ncolors -ge 8; then
        export bold="$(tput bold)"
        export underline="$(tput smul)"
        export nounderline="$(tput rmul)"
        export standout="$(tput smso)"
        export normal="\033[0m"
        export normal="$(tput sgr0)"
        export black="$(tput setaf 0)"
        export red="$(tput setaf 1)"
        export green="$(tput setaf 2)"
        export yellow="$(tput setaf 3)"
        export blue="$(tput setaf 4)"
        export magenta="$(tput setaf 5)"
        export cyan="$(tput setaf 6)"
        export white="$(tput setaf 7)"
    fi
fi

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"

# Because of duplicats from .zprofile the make it array + unique
typeset -aU path
