{ config, pkgs, ... }:

{
# List packages installed in system profile. To search by name, run:
# $ nix-env -qaP | grep wget
environment.systemPackages = with pkgs; [
	vim
	coreutils  curl wget cloc
	jq yq
	awscli2
	graphviz

	watch
	ranger mc tree
	pwgen

	git tig gh
	gnupg
	iterm2
	alacritty
	fd ripgrep fzf peco entr
	go
	kubecolor
    k9s
    cowsay
    tealdeer

	bat
	drawio
	# bitwarden
	# sublime4
	# sublime-merge
];

security.pam.enableSudoTouchIdAuth = true;

# Use a custom configuration.nix location.
# $ darwin-rebuild switch -I darwin-config=$HOME/.config/nixpkgs/darwin/configuration.nix
# environment.darwinConfig = "$HOME/.config/nixpkgs/darwin/configuration.nix";

# Auto upgrade nix package and the daemon service.
services.nix-daemon.enable = true;
# nix.package = pkgs.nix;

# Create /etc/zshrc that loads the nix-darwin environment.
programs.zsh.enable = true;  # default shell on catalina
# programs.fish.enable = true;

# Used for backwards compatibility, please read the changelog before changing.
# $ darwin-rebuild changelog
system.stateVersion = 4;
}
