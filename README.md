# dotfiles

This is my current state of my dotfiles.

## Prerequisites

Oh My Zsh:

    sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

## To install

    git clone --bare gitlab.com:sorennielsen/dotfiles $HOME/.dotfiles
    git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME checkout
    git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME config --local status.showUntrackedFiles no

## To use

An alias `config` is setup to work with the dotfiles.

Simply use it as you would with Git:

    config status
    config add .some-file
    config commit -m "Changed someting"
    config push

## To use Tig

Another alias `ctig`  is created to make Tig work with this setup.

